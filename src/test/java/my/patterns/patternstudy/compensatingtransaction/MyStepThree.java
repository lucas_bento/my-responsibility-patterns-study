package my.patterns.patternstudy.compensatingtransaction;

import my.patterns.patternstudy.compensatingtransaction.exception.StepThreeException;

public class MyStepThree extends StepThree {
	
	private boolean success = true;
	
	private boolean perform = false;
	
	private boolean compensate = false;
	
	@Override
	public void perform() throws StepThreeException {
		
		if (!success) {
			throw new StepThreeException("Exception on step one");
		}
		super.perform();
		perform = true;
	}

	@Override
	public void compensate() {
		super.compensate();
		compensate = true;
	}
	

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public boolean isPerform() {
		return perform;
	}

	public void setPerform(boolean perform) {
		this.perform = perform;
	}

	public boolean isCompensate() {
		return compensate;
	}

	public void setCompensate(boolean compensate) {
		this.compensate = compensate;
	}

	
}
