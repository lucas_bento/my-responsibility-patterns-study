package my.patterns.patternstudy.compensatingtransaction;

import my.patterns.patternstudy.compensatingtransaction.exception.StepTwoException;

public class MyStepTwo extends StepTwo {
	
	private boolean success = true;
	
	private boolean perform = false;
	
	private boolean compensate = false;
	
	@Override
	public void perform() throws StepTwoException {
		
		if (!success) {
			throw new StepTwoException("Exception on step one");
		}
		super.perform();
		perform = true;
	}

	@Override
	public void compensate() {
		super.compensate();
		compensate = true;
	}
	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public boolean isPerform() {
		return perform;
	}

	public void setPerform(boolean perform) {
		this.perform = perform;
	}

	public boolean isCompensate() {
		return compensate;
	}

	public void setCompensate(boolean compensate) {
		this.compensate = compensate;
	}

}
