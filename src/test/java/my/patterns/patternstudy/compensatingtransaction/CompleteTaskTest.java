package my.patterns.patternstudy.compensatingtransaction;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CompleteTaskTest {

	@Test
	public void test_doSomething_success() {
		
		MyStepOne one = new MyStepOne();
		one.setSuccess(true);
		
		MyStepTwo two = new MyStepTwo();
		two.setSuccess(true);
		
		MyStepThree three = new MyStepThree();
		three.setSuccess(true);
		
		CompleteTask task = new CompleteTask(one, two, three);
		task.doSomething();
		
		assertTrue(one.isPerform());
		assertTrue(two.isPerform());
		assertTrue(three.isPerform());
		
		assertFalse(one.isCompensate());
		assertFalse(two.isCompensate());
		assertFalse(three.isCompensate());
	}

	@Test
	public void test_doSomething_stepOneFailure() {
		
		MyStepOne one = new MyStepOne();
		one.setSuccess(false);
		
		MyStepTwo two = new MyStepTwo();
		two.setSuccess(true);
		
		MyStepThree three = new MyStepThree();
		three.setSuccess(true);
		
		CompleteTask task = new CompleteTask(one, two, three);
		task.doSomething();
		
		assertFalse(one.isPerform());
		assertFalse(two.isPerform());
		assertFalse(three.isPerform());
		
		assertFalse(one.isCompensate());
		assertFalse(two.isCompensate());
		assertFalse(three.isCompensate());
	}
	
	@Test
	public void test_doSomething_stepTwoFailure() {
		
		MyStepOne one = new MyStepOne();
		one.setSuccess(true);
		
		MyStepTwo two = new MyStepTwo();
		two.setSuccess(false);
		
		MyStepThree three = new MyStepThree();
		three.setSuccess(true);
		
		CompleteTask task = new CompleteTask(one, two, three);
		task.doSomething();
		
		assertTrue(one.isPerform());
		assertFalse(two.isPerform());
		assertFalse(three.isPerform());
		
		assertTrue(one.isCompensate());
		assertFalse(two.isCompensate());
		assertFalse(three.isCompensate());
	}
	
	@Test
	public void test_doSomething_stepThreeFailure() {
		
		MyStepOne one = new MyStepOne();
		one.setSuccess(true);
		
		MyStepTwo two = new MyStepTwo();
		two.setSuccess(true);
		
		MyStepThree three = new MyStepThree();
		three.setSuccess(false);
		
		CompleteTask task = new CompleteTask(one, two, three);
		task.doSomething();
		
		assertTrue(one.isPerform());
		assertTrue(two.isPerform());
		assertFalse(three.isPerform());
		
		assertTrue(one.isCompensate());
		assertTrue(two.isCompensate());
		assertFalse(three.isCompensate());
	}

}
