package my.patterns.patternstudy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PatternstudyApplication {

	public static void main(String[] args) {
		SpringApplication.run(PatternstudyApplication.class, args);
	}
}
