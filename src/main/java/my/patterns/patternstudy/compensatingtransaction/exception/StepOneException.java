package my.patterns.patternstudy.compensatingtransaction.exception;

public class StepOneException extends CompensableException {

	private static final long serialVersionUID = -3577973184576706693L;

	public StepOneException(String message) {
		super(message);
	}
}
