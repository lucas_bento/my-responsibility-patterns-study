package my.patterns.patternstudy.compensatingtransaction.exception;

public class StepTwoException extends CompensableException {

	private static final long serialVersionUID = 6145447442829246296L;

	public StepTwoException(String message) {
		super(message);
	}
}
