package my.patterns.patternstudy.compensatingtransaction.exception;

public class StepThreeException extends CompensableException {

	private static final long serialVersionUID = 3621784411998561996L;

	public StepThreeException(String message) {
		super(message);
	}
}
