package my.patterns.patternstudy.compensatingtransaction;

import org.springframework.beans.factory.annotation.Autowired;

import my.patterns.patternstudy.compensatingtransaction.exception.StepOneException;
import my.patterns.patternstudy.compensatingtransaction.exception.StepThreeException;
import my.patterns.patternstudy.compensatingtransaction.exception.StepTwoException;

public class CompleteTask {

	private StepOne one;
	
	private StepTwo two;
	
	private StepThree three;
	
	@Autowired
	public CompleteTask(StepOne one, StepTwo two, StepThree three) {
		this.one = one;
		this.two = two;
		this.three = three;
	}

	public void doSomething() {

		try {
			one.perform();
			two.perform();
			three.perform();
		} catch (StepOneException e) {
			// nothing to undo
		} catch (StepTwoException e) {
			one.compensate();
		} catch (StepThreeException e) {
			one.compensate();
			two.compensate();
		}

	}

}