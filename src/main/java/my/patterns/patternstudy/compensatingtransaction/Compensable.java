package my.patterns.patternstudy.compensatingtransaction;

import my.patterns.patternstudy.compensatingtransaction.exception.CompensableException;

public interface Compensable {

	public void perform() throws CompensableException;

	public void compensate();

}
